#!/usr/bin/env node

const program = require('commander');
const { version } = require('../package.json');

program
  .version(version)
  .command('create', 'create a release')
  .command('config', 'operate configuration')
  .parse(process.argv);

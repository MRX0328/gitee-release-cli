#!/usr/bin/env node

const program = require('commander');
const prompts = require('prompts');
const { execSync } = require('child_process');
const logger = require('../src/logger');
const {
  collect, getNextVersion, generate, upload,
} = require('../src/index');

async function run() {
  let response;
  const data = await collect(program.latestVersion);
  let version = getNextVersion(data);

  logger.log(`current version contains ${data.changes.breakingChanges} breaking changes,`
            + ` ${data.changes.features} features, ${data.changes.bugs} bug fixes.`);
  logger.log(`version will update from ${data.latestVersion} to ${version}`);

  if (program.versionSelection) {
    response = await prompts([
      {
        name: 'selectVersion',
        type: 'select',
        message: `Do you want to set the new version to ${version}?`,
        choices: [
          { title: 'Yes', description: 'Use this version', value: 'yes' },
          { title: 'No', description: 'Use custom version', value: 'no' },
          { title: 'Cancel', value: 'cancel' },
        ],
      },
      {
        type: (prev) => (prev === 'no' ? 'text' : null),
        name: 'version',
        message: 'Please input version',
      },
    ]);
    if (response.selectVersion === 'cancel') {
      return;
    }
    if (response.selectVersion === 'no') {
      version = response.version;
    }
  }

  const release = generate({
    ...data,
    version,
    prerelease: program.prerelease,
    latestVersion: program.latestVersion,
  });

  if (program.withNpmVersion) {
    logger.log('update the version to package.json');
    execSync(`npm version ${release.tag_name} -m "chore(release): ${release.tag_name}"`, { stdio: 'inherit' });
  }
  if (program.withGitPush) {
    logger.log('push commits and tags to remote');
    execSync('git push origin master --tags', { stdio: 'inherit' });
  }
  if (!program.upload) {
    logger.log(release);
    response = await prompts({
      name: 'upload',
      type: 'confirm',
      message: 'Do you want to upload the above?',
    });
    if (response.upload) {
      upload(release);
    }
  } else {
    upload(release);
  }
}

program
  .option('--with-npm-version', 'Run `npm version` to update the version to package.json')
  .option('--with-git-push', 'Run `git push` to push commits and tags to remote')
  .option('--prerelease <identifiers>', 'Add prerelease tag, identifiers can be alpha or beta', false)
  .option('--upload', 'Upload the release to Gitee.com without prompting')
  .option('--latest-version <version>', 'Specify the latest version and let the next version start incrementing from it.')
  .option('--no-version-selection', 'No prompting for version selection')
  .action(run)
  .parse(process.argv);
